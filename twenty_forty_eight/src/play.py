from . import board as game_board


def display_intro():
    print()
    print('*'*32)
    print()
    print('Welcome to an implementation of the popular game 2048.')
    print()
    print('Swipe along any direction on the board.')
    print('To collapse the board in that direction.')
    print()
    print('Have Fun!')
    print()
    print('*'*32)

def display_menu():
    print()
    print('*'*16)
    print()
    print('**MENU**')
    print()
    print('='*16)
    print('Use the keys on your numeric keypad to perform swipe actions.')
    print('OR')
    print('Just use the alphabet')
    display_short_menu()
    print('='*16)

def display_short_menu():
    print()
    print('-'*16)
    print('Right --> R or 6')
    print('Left  --> L or 4')
    print('Up    --> U or 8')
    print('Down  --> D or 2')
    print('-'*16)
    print('Menu  --> M')
    print('-'*16)
    print()
    print('*'*16)


def play():

    display_intro()

    while True:
        print()
        N = input("What size of board do you want to play? 4 to 8: ").strip()[0]
        if N in ('4', '5', '6', '7', '8'):
            break
        print()
        print("Invalid board size: ", N)

    board = game_board.Board(int(N))
    print()
    print(board)
    display_menu()

    while True:
        print()
        d = input("Enter a command from the menu: ").strip()[0].upper()
        if   d in ('R', '6'):
            board.swipe_right()
        elif d in ('L', '4'):
            board.swipe_left()
        elif d in ('U', '8'):
            board.swipe_up()
        elif d in ('D', '2'):
            board.swipe_down()
        elif d == 'M':
            display_short_menu()
            continue
        else:
            print()
            print("Invalid command: ", d)
            continue


        print()
        print(board)

        if not board._is_reducible():
            break

    print()
    print('GAME OVER')
    print()
    resp = input("Do you wish to play again? Y or N: ").strip()[0].upper()

    if resp == 'Y':
        play()

    else:
        print()
        resp = input("Quit for real? Y or N: ").strip()[0].upper()

        if resp == 'N':
            play()

    return

if __name__ == '__main__':
    play()
