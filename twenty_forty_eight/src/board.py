import random
import copy
import math


class Board:

    def __init__(self, n=4):
        '''A board is an NxN 2d-grid.

        N is the size of the board.
        For a classic board, N==4.'''
        self._size = n
        self._grid = list([0] * n for i in range(n))
        self._populate_random_blank_cell()

    def grid(self):
        return copy.deepcopy(self._grid)

    def size(self):
        return self._size

    def _populate_random_blank_cell(self):
        '''Puts a 2 in a random cell having 0 in it.

        Assumes there is at least one blank cell on board.

        Returns a tuple of the row number and column number 
        of the cell.'''
        i = self._get_index_of_random_non_filled_row()
        j = self._get_index_of_random_blank_cell_in_nth_row(i)

        self._grid[i][j] = 2

        return (i, j)

    def _get_index_of_random_non_filled_row(self):
        '''Returns the row number of a random row on board, 
        containing at least one blank cell in it.

        Assumes such a row exists on board.'''
        nums = []
        for (i, row) in enumerate(self._grid):
            if 0 in row:
                nums.append(i)
        return random.choice(nums)

    def _get_index_of_random_blank_cell_in_nth_row(self, n):
        '''Returns the index of a random cell containing a 0 
        in the nth row on board.

        Assumes such a cell exists on such a row.'''
        indices = []
        for (i, elt) in enumerate(self._grid[n]):
            if elt == 0:
                indices.append(i)
        return random.choice(indices)

    def _is_nth_row_left_to_right_reducible(self, n):
        '''Returns whether the nth row on board can be 
        collapsed left-to-right, 2048 style.

        A row can be collapsed left-to-right if any 
        of the following holds:
        * there is a blank cell to the right of a 
          non-blank cell
        * two non-blank cell neighbours contain the 
          same element
        '''
        right_idx = self._size - 1
        left_idx  = right_idx - 1

        while left_idx >= 0:
            right_elt = self._grid[n][right_idx]
            left_elt  = self._grid[n][left_idx]

            if right_elt == 0:
                if left_elt == 0:
                    # two blank cell, move on
                    pass
                else:
                    # a blank cell to the right of 
                    # a non-blank cell
                    return True
            else:
                if right_elt == left_elt:
                    # two similar non-blank neighbours
                    return True

            right_idx -= 1
            left_idx  -= 1

        return False

    def _is_left_to_right_reducible(self):
        '''Returns whether board can be collapsed 
        left-to-right, 2048 style.'''
        for i in range(self._size):
            if self._is_nth_row_left_to_right_reducible(i):
                return True
        return False

    def _is_reducible(self):
        '''Returns whether the board can be collapsed in 
        any direction at all, as in the game 2048.

        NOTE TO SELF (and whomever else it may concern):
        * use only for checking whether game is over.
        * use for only checking whether game is over.
        * re-read this note until you get it.'''

        # take an element
        # test whether it is a zero, or it can be 
        # merged with either of its neighbours
        # to the east and to the south
        # that's all
        # and I'm feeling so smug right now

        for i in range(self._size):
            for j in range(self._size):
                if self._grid[i][j] == 0:
                    return True

                # and because they say to rather
                # beg pardon than ask permission

                # peek at the east
                try:
                    if self._grid[i][j] == self._grid[i][j + 1]:
                        return True
                except IndexError:
                    pass

                # peek at the south
                try:
                    if self._grid[i][j] == self._grid[i + 1][j]:
                        return True
                except IndexError:
                    pass

        return False

    def _reduce_left_to_right(self):
        '''Collapses every row on board from left to right,
        2048 style.

        Does not check for reducibility.'''
        for i in range(self._size):
            self._reduce_nth_row_left_to_right(i)

    def _reduce_nth_row_left_to_right(self, n):
        '''Collapses the nth row on board from left to right,
        2048 style.

        Does not check for reducibility.'''
        self._grid[n] = self._right_aligned(
                            self._stripped(
                                self._reduced(
                                    self._stripped(self._grid[n]))))

    def _stripped(self, row):
        '''Returns a copy of argument row, with all 
        zeros filtered out of it.'''
        return list(filter(lambda x: x != 0, row))

    def _reduced(self, row):
        '''Returns a 2048-styled left-to-right collapsed 
        copy of a row of NON-BLANK elements.'''
        res = row[:]

        z = len(res) - 1
        y = z - 1

        while y >= 0:
            if res[z] == res[y]:
                res[z] *= 2
                res[y]  = 0

                z -= 2
                y -= 2

            else:
                z -= 1
                y -= 1

        return res 

    def _right_aligned(self, row):
        '''Returns a zero leading list of length n containing 
        the elements of the input list, all flushed right.'''
        offset = self._size - len(row)
        res = [0]*offset
        res.extend(row)
        return res

    def _rotate_90(self):
        '''Rotates board through 90 degrees in the anticlockwise 
        direction. In place.

                BEFORE                          AFTER
        [ [ 1,  2,  3,  4,  ]           [ [ 4, 8, 12, 16, ]     
        , [ 5,  6,  7,  8,  ]           , [ 3, 7, 11, 15, ]
        , [ 9,  10, 11, 12, ]           , [ 2, 6, 10, 14, ]
        , [ 13, 14, 15, 16, ] ]         , [ 1, 5, 9,  13, ] ]

        Assumes board is SQUARE.

        Rotating a board by 90 makes it possible to 
        simulate the top-to-bottom collapse of a board using 
        self.reduce_rows().'''
        N = self._size
        for i in range(int(N / 2)):
            for j in range(i, N - 1 - i):
                ( self._grid[i]         [j]
                , self._grid[j]         [N - 1 - i]
                , self._grid[N - 1 - i] [N - 1 - j]
                , self._grid[N - 1 -j]  [i]
                ) = ( self._grid[j]         [N - 1 - i]
                    , self._grid[N - 1 - i] [N - 1 - j]
                    , self._grid[N - 1 -j]  [i]
                    , self._grid[i]         [j]
                    )

    def _rotate_180(self):
        '''Rotates board through 180 degrees in the anticlockwise 
        direction. In place.

                BEFORE                          AFTER
        [ [ 1,  2,  3,  4,  ]           [ [ 16, 15, 14, 13, ]     
        , [ 5,  6,  7,  8,  ]           , [ 12, 11, 10,  9, ]
        , [ 9,  10, 11, 12, ]           , [ 8,   7,  6,  5, ]
        , [ 13, 14, 15, 16, ] ]         , [ 4,   3,  2,  1, ] ]

        Assumes board is SQUARE.

        Rotating a board by 180 makes it possible to 
        simulate the right-to-left collapse of a board using 
        self.reduce_rows().'''

        N = self._size

        # reverse rows
        for i in range(N):
            for j in range(int(N/2)):
                ( self._grid[i][j]
                , self._grid[i][N - 1 - j]
                ) = ( self._grid[i][N - 1 - j]
                    , self._grid[i][j]
                    )
        # reverse columns
        for j in range(N):
            for i in range(int(N/2)):
                ( self._grid[i][j]
                , self._grid[N - 1 - i][j]
                ) = ( self._grid[N - 1 - i][j]
                    , self._grid[i][j]
                    )

    def _rotate_270(self):
        '''Rotates board through 270 degrees in the anticlockwise 
        direction. In place.

                BEFORE                          AFTER
        [ [ 1,  2,  3,  4,  ]           [ [ 13, 9,  5, 1, ]     
        , [ 5,  6,  7,  8,  ]           , [ 14, 10, 6, 2, ]
        , [ 9,  10, 11, 12, ]           , [ 15, 11, 7, 3, ]
        , [ 13, 14, 15, 16, ] ]         , [ 16, 12, 8, 4, ] ]

        Assumes board is SQUARE.

        Rotating a board by 270 makes it possible to 
        simulate the bottom-to-top collapse of a board using 
        self.reduce_rows().'''
        N = self._size
        for i in range(int(N/2)):
            for j in range(i, N - 1 - i):
                ( self._grid[i]        [j]
                , self._grid[N - 1 - j][i]
                , self._grid[N - 1 - i][N - 1 - j]
                , self._grid[j]        [N - 1 - i]
                ) = ( self._grid[N - 1 - j][i]
                    , self._grid[N - 1 - i][N - 1 - j]
                    , self._grid[j]        [N - 1 - i]
                    , self._grid[i]        [j]
                    )

    def swipe_right(self):
        '''Performs the action of swiping left to right 
        on a 2048 game board.

        Returns the index of new addition to board, if any.'''
        if not self._is_left_to_right_reducible():
            return (None, None)
        self._reduce_left_to_right()
        return self._populate_random_blank_cell()

    def swipe_left(self):
        '''Performs the action of swiping left to right 
        on a 2048 game board.

        Returns the index of new addition to board, if any.'''
        self._rotate_180()

        if not self._is_left_to_right_reducible():
            self._rotate_180()
            return (None, None)

        self._reduce_left_to_right()
        self._rotate_180()
        return self._populate_random_blank_cell()

    def swipe_up(self):
        '''Performs the action of swiping bottom to top 
        on a 2048 game board.

        Returns the index of new addition to board, if any.'''
        self._rotate_270()

        if not self._is_left_to_right_reducible():
            self._rotate_90()
            return (None, None)

        self._reduce_left_to_right()
        self._rotate_90()
        return self._populate_random_blank_cell()

    def swipe_down(self):
        '''Performs the action of swiping top to bottom 
        on a 2048 game board.

        Returns the index of new addition to board, if any.'''
        self._rotate_90()

        if not self._is_left_to_right_reducible():
            self._rotate_270()
            return (None, None)

        self._reduce_left_to_right()
        self._rotate_270()
        return self._populate_random_blank_cell()

    def can_change(self):
        '''Returns whether configuration of elements on 
        board can be changed by any type of swipe action.'''
        return self._is_reducible()

    def __str__(self):
        # the span of each cell should be the number 
        # of digits in the largest element on board
        # max_elt = max(map(max, self._grid))
        # span    = (1 if max_elt == 0 else int(math.log10(max_elt)) + 1)

        def show_before_row():
            return ''.join(f' {" "*4} |' for i in range(self._size))

        def show_row(row):
            return ''.join(f' {elt:>4} |' for elt in row)

        def show_after_row():
            return ''.join(f'_{"_"*4}_|' for i in range(self._size))

        return '\n'.join(
            '\n'.join([show_before_row(), show_row(row), show_after_row()]) for row in self._grid)
