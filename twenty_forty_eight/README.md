# Twenty Forty Eight

An implementation of the logic for [the game 2048](https://en.wikipedia.org/wiki/2048_(video_game)) in Python.

## Setup Instructions

Clone the repository

    $ git clone https://bitbucket.org/mfonism/python_weekly_projects.git

This will download the parent repository to your computer.

cd into this project's directory.

    $ cd python_weekly_projects/twenty_forty_eight/

run the tests

    $ python -m unittest discover

Invoke the python shell

    $ python

In the shell, import the `play` function from the src package

    >>> from src import play

Play with it

    >>> play()

## Reflection

This was a week long personal project aimed at helping me level-up in OOP and TDD.

I learnt how to break my code into small, reusable modules. And I've started seeing some of the payoffs of TDD - safer refactorings, less time spent tracking bugs, tidier code.
