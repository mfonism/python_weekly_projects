import unittest
import random

from src.board import Board


class BoardTest(unittest.TestCase):

    def test_initial_grid_default_dimension(self):
        board = Board()
        num_cells = sum(map(len, board.grid()))

        self.assertEqual(num_cells, 16)

    def test_initial_grid_dimensions(self):
        n = random.randint(4,8)
        board = Board(n)

        num_cells = sum(map(len, board.grid()))

        self.assertEqual(board.size(), n)
        self.assertEqual(num_cells, board.size()**2)

    def test_initial_grid_content(self):
        # every cell in the initial grid should contain a 0
        # except one random cell, which contains a 2
        board = Board()

        histogram = {}
        for row in board.grid():
            for cell_content in row:
                histogram[cell_content] = histogram.get(cell_content, 0) + 1

        self.assertDictEqual(histogram, {2: 1, 0: (4*4)-1 })

    def test_grid_property_does_not_mutate_board(self):
        board = Board()
        board.grid()[0] = []

        self.assertNotEqual(board.grid()[0], [])
        self.assertEqual(len(board.grid()[0]), 4)


class RowReducibilityTest(unittest.TestCase):
    '''Tests the left-to-right reducibility of rows.'''

    def setUp(self):
        self.board = Board(6)
        self.board._grid = [ [ 8, 8, 2, 4, 8, 2 ]
                           , [ 2, 8, 4, 2, 8, 2 ]
                           , [ 2, 0, 8, 4, 8, 2 ]
                           , [ 8, 2, 4, 8, 2, 0 ]
                           , [ 0, 0, 0, 2, 8, 2 ]
                           , [ 0, 0, 0, 0, 0, 0 ]
                           , ]

    def test_is_reducible_if_any_two_neighbours_are_similar(self):
        self.assertTrue(self.board._is_nth_row_left_to_right_reducible(0))

    def test_is_not_reducible_if_filled_and_no_two_neigbours_are_similar(self):
        self.assertFalse(self.board._is_nth_row_left_to_right_reducible(1))

    def test_is_reducible_if_it_has_a_blank_island(self):
        self.assertTrue(self.board._is_nth_row_left_to_right_reducible(2))

    def test_is_reducible_if_it_has_right_most_zeros(self):
        self.assertTrue(self.board._is_nth_row_left_to_right_reducible(3))

    def test_right_aligned_row_is_not_reducible_if_no_two_non_blank_neighbours_are_similar(self):
        self.assertFalse(self.board._is_nth_row_left_to_right_reducible(4))

    def test_blank_row_is_not_reducible(self):
        self.assertFalse(self.board._is_nth_row_left_to_right_reducible(5))


class RowReductionTest(unittest.TestCase):
    '''Tests the action of reducing a row in the 
    left-to-right direction.'''

    def setUp(self):
        self.board = Board()
        self.NUMS = [ 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024 ]

    def test_reduce_nth_row_left_to_right__single_num(self):
        # a row with a single element at any point in the row
        # reduces to a row with that element at the end of the row
        x = random.choice(self.NUMS)
        row         = [ 0, 0, x, 0, ]
        reduced_row = [ 0, 0, 0, x, ]

        random.shuffle(row)

        self.board._grid[3] = row
        self.board._reduce_nth_row_left_to_right(3)

        self.assertListEqual(
            self.board._grid[3],
            reduced_row,
            msg=f'{row} did not reduce (LTR) to '
                f'{reduced_row} as was expected, '
                f'Instead, it ended as {self.board._grid[3]}')

    def test_reduce_nth_row_left_to_right__pair_of_num(self):
        # a row with a pair of the same element at any point
        # in the row reduces to a row with
        # double that element at the end of the row
        x = random.choice(self.NUMS)
        row         = [ x, 0, x, 0, ]
        reduced_row = [ 0, 0, 0, 2*x, ]

        random.shuffle(row)

        self.board._grid[3] = row
        self.board._reduce_nth_row_left_to_right(3)

        self.assertListEqual(
            self.board._grid[3],
            reduced_row,
            msg=f'{row} did not reduce (LTR) to '
                f'{reduced_row} as was expected, '
                f'Instead, it ended as {self.board._grid[3]}')

    def test_reduce_nth_row_left_to_right__triple_of_num(self):
        # a row with a three of the same elements at any point
        # in the row reduces to a row with
        # double that element at the end of the row
        # and one time that element preceding it
        x = random.choice(self.NUMS)
        row         = [ x, x, 0, x, ]
        reduced_row = [ 0, 0, x, 2*x, ]

        random.shuffle(row)

        self.board._grid[3] = row
        self.board._reduce_nth_row_left_to_right(3)

        self.assertListEqual(
            self.board._grid[3],
            reduced_row,
            msg=f'{row} did not reduce (LTR) to '
                f'{reduced_row} as was expected, '
                f'Instead, it ended as {self.board._grid[3]}')

    def test_reduce_nth_row_left_to_right__all_four(self):
        # a row with a four of the same elements at any point
        # in the row reduces to a row with
        # double that element at the end of the row
        # and double that same element preceding it
        x = random.choice(self.NUMS)
        row         = [ x, x, x, x, ]
        reduced_row = [ 0, 0, 2*x, 2*x, ]

        random.shuffle(row)

        self.board._grid[3] = row
        self.board._reduce_nth_row_left_to_right(3)

        self.assertListEqual(
            self.board._grid[3],
            reduced_row,
            msg=f'{row} did not reduce (LTR) to '
                f'{reduced_row} as was expected, '
                f'Instead, it ended as {self.board._grid[3]}')


class RowReductionHelperTest(unittest.TestCase):
    '''Tests the helper functions which were composed 
    to create the definition of the nth row reduction 
    property.'''
    def test_reduced__2(self):
        self.assertListEqual(Board()._reduced([ 2, ]), [ 2, ])

    def test_reduced__22(self):
        self.assertListEqual(Board()._reduced([ 2, 2, ]), [ 0, 4, ])

    def test_reduced__222(self):
        self.assertListEqual(Board()._reduced([ 2, 2, 2, ]), [ 2, 0, 4, ])

    def test_reduced__2222(self):
        self.assertListEqual(Board()._reduced([ 2, 2, 2, 2, ]), [ 0, 4, 0, 4, ])

    def test_reduced__2244(self):
        self.assertListEqual(Board()._reduced([ 2, 2, 4, 4, ]), [ 0, 4, 0, 8, ])

    def test_reduced__422(self):
        self.assertListEqual(Board()._reduced([ 4, 2, 2, ]), [ 4, 0, 4, ])

    def test_reduced__224(self):
        self.assertListEqual(Board()._reduced([ 2, 2, 4, ]), [ 0, 4, 4, ])

    def test_reduced__2442(self):
        self.assertListEqual(Board()._reduced([ 2, 4, 4, 2, ]), [ 2, 0, 8, 2, ])

    def test_reduced__2424(self):
        self.assertListEqual(Board()._reduced([ 2, 4, 2, 4, ]), [ 2, 4, 2, 4, ])

    def test_right_aligned__2(self):
        self.assertListEqual(Board()._right_aligned([2]), [ 0, 0, 0, 2])

    def test_right_aligned__24(self):
        self.assertListEqual(Board()._right_aligned([ 2, 4 ]), [ 0, 0, 2, 4 ])

    def test_right_aligned__248(self):
        self.assertListEqual(Board()._right_aligned([ 2, 4, 8 ]), [ 0, 2, 4, 8 ])

    def test_right_aligned__2484(self):
        self.assertListEqual(Board()._right_aligned([ 2, 4, 8, 4 ]), [ 2, 4, 8, 4 ])


class BoardRotationTest(unittest.TestCase):

    def setUp(self):
        self.board = Board(5)
        self.board._grid = [ [  1,  2,  3,  4,  5, ]
                           , [  6,  7,  8,  9, 10, ]
                           , [ 11, 12, 13, 14, 15, ]
                           , [ 16, 17, 18, 19, 20, ]
                           , [ 21, 22, 23, 24, 25, ]
                           ]

    def test_rotate_90(self):
        self.board._rotate_90()
        self.assertEqual(self.board._grid,
                      [ [ 5, 10, 15, 20, 25, ]
                      , [ 4,  9, 14, 19, 24, ]
                      , [ 3,  8, 13, 18, 23, ]
                      , [ 2,  7, 12, 17, 22, ]
                      , [ 1,  6, 11, 16, 21, ]
                      ])

    def test_rotate_180(self):
        self.board._rotate_180()
        self.assertEqual(self.board._grid,
                      [ [ 25, 24, 23, 22, 21, ]
                      , [ 20, 19, 18, 17, 16, ]
                      , [ 15, 14, 13, 12, 11, ]
                      , [ 10,  9,  8,  7,  6, ]
                      , [ 5,   4,  3,  2,  1, ]
                      ])

    def test_rotate_270(self):
        self.board._rotate_270()
        self.assertEqual(self.board._grid,
                      [ [ 21, 16, 11,  6, 1, ]
                      , [ 22, 17, 12,  7, 2, ]
                      , [ 23, 18, 13,  8, 3, ]
                      , [ 24, 19, 14,  9, 4, ]
                      , [ 25, 20, 15, 10, 5, ]
                      ])

    def test_rotating_by_90_then_270_is_identity(self):
        clone_board = Board()
        clone_board._grid = self.board.grid()

        self.assertEqual(self.board._grid, clone_board._grid)

        clone_board._rotate_90()
        self.assertNotEqual(self.board._grid, clone_board._grid)

        clone_board._rotate_270()
        self.assertEqual(self.board._grid, clone_board._grid)

    def test_rotating_by_180_then_180_is_identity(self):
        clone_board = Board()
        clone_board._grid = self.board.grid()

        self.assertEqual(self.board._grid, clone_board._grid)

        clone_board._rotate_180()
        self.assertNotEqual(self.board._grid, clone_board._grid)

        clone_board._rotate_180()
        self.assertEqual(self.board._grid, clone_board._grid)

    def test_rotating_by_270_then_90_is_identity(self):
        clone_board = Board()
        clone_board._grid = self.board.grid()

        self.assertEqual(self.board._grid, clone_board._grid)

        clone_board._rotate_270()
        self.assertNotEqual(self.board._grid, clone_board._grid)

        clone_board._rotate_90()
        self.assertEqual(self.board._grid, clone_board._grid)


class BoardReductionTest(unittest.TestCase):
    '''Tests all board wide reduction / swipe actions.'''

    def setUp(self):
        self.board = Board(6)
        self.board._grid = [ [ 8, 8, 2, 4, 8, 2 ]
                           , [ 2, 8, 4, 2, 8, 2 ]
                           , [ 2, 0, 8, 4, 8, 2 ]
                           , [ 8, 2, 4, 8, 2, 0 ]
                           , [ 0, 0, 0, 2, 8, 2 ]
                           , [ 0, 0, 0, 0, 0, 0 ]
                           , ]

    def test_reduce_left_to_right(self):
        self.board._reduce_left_to_right()
        self.assertListEqual(self.board._grid,
            [ [ 0, 16, 2, 4, 8, 2 ]
            , [ 2,  8, 4, 2, 8, 2 ]
            , [ 0,  2, 8, 4, 8, 2 ]
            , [ 0,  8, 2, 4, 8, 2 ]
            , [ 0,  0, 0, 2, 8, 2 ]
            , [ 0,  0, 0, 0, 0, 0 ]
            , ])

    def test_swipe_right(self):
        (i,j) = self.board.swipe_right()
        res   = [ [ 0, 16, 2, 4, 8, 2 ]
                , [ 2,  8, 4, 2, 8, 2 ]
                , [ 0,  2, 8, 4, 8, 2 ]
                , [ 0,  8, 2, 4, 8, 2 ]
                , [ 0,  0, 0, 2, 8, 2 ]
                , [ 0,  0, 0, 0, 0, 0 ]
                , ]

        self.assertEqual(res[i][j], 0)
        res[i][j] = 2

        self.assertListEqual(self.board._grid, res)

    def test_swipe_left(self):
        (i,j) = self.board.swipe_left()
        res   = [ [ 16, 2, 4, 8, 2, 0 ]
                , [ 2,  8, 4, 2, 8, 2 ]
                , [ 2,  8, 4, 8, 2, 0 ]
                , [ 8,  2, 4, 8, 2, 0 ]
                , [ 2,  8, 2, 0, 0, 0 ]
                , [ 0,  0, 0, 0, 0, 0 ]
                , ]

        self.assertEqual(res[i][j], 0)
        res[i][j] = 2

        self.assertListEqual(self.board._grid, res)

    def test_swipe_up(self):
        (i,j) = self.board.swipe_up()
        res   = [ [ 8, 16, 2, 4, 16, 4 ]
                , [ 4,  2, 4, 2,  8, 4 ]
                , [ 8,  0, 8, 4,  2, 0 ]
                , [ 0,  0, 4, 8,  8, 0 ]
                , [ 0,  0, 0, 2,  0, 0 ]
                , [ 0,  0, 0, 0,  0, 0 ]
                , ]

        self.assertEqual(res[i][j], 0)
        res[i][j] = 2

        self.assertListEqual(self.board._grid, res)

    def test_swipe_down(self):
        (i,j) = self.board.swipe_down()
        res   = [ [ 0,  0, 0, 0,  0, 0 ]
                , [ 0,  0, 0, 4,  0, 0 ]
                , [ 0,  0, 2, 2,  8, 0 ]
                , [ 8,  0, 4, 4, 16, 0 ]
                , [ 4, 16, 8, 8,  2, 4 ]
                , [ 8,  2, 4, 2,  8, 4 ]
                , ]

        self.assertEqual(res[i][j], 0)
        res[i][j] = 2

        self.assertListEqual(self.board._grid, res)


class SwipeUnsuccessfulTest(unittest.TestCase):
    '''Tests that no element is added to board after a 
    swipe which did not result in any reduction on board.'''

    def setUp(self):
        self.board = Board(6)
        # make it an impossible kid
        self.board._grid = [ [ 8, 4, 2, 4, 8, 2 ]
                           , [ 2, 8, 4, 2, 4, 8 ]
                           , [ 4, 2, 8, 4, 8, 2 ]
                           , [ 8, 4, 2, 8, 2, 4 ]
                           , [ 4, 2, 8, 2, 4, 8 ]
                           , [ 8, 4, 2, 8, 2, 4 ]
                           , ]

        # make a clone
        self.clone_board = Board(6)
        self.clone_board._grid = self.board.grid()

    def test_swipe_right_not_successful(self):
        (i,j) = self.board.swipe_right()

        self.assertIsNone(i)
        self.assertIsNone(j)
        self.assertEqual(self.board._grid, self.clone_board._grid)

    def test_swipe_left_not_successful(self):
        (i,j) = self.board.swipe_left()

        self.assertIsNone(i)
        self.assertIsNone(j)
        self.assertEqual(self.board._grid, self.clone_board._grid)

    def test_swipe_up_not_successful(self):
        (i,j) = self.board.swipe_up()

        self.assertIsNone(i)
        self.assertIsNone(j)
        self.assertEqual(self.board._grid, self.clone_board._grid)

    def test_swipe_down_not_successful(self):
        (i,j) = self.board.swipe_down()

        self.assertIsNone(i)
        self.assertIsNone(j)
        self.assertEqual(self.board._grid, self.clone_board._grid)


class BoardReducibilityTest(unittest.TestCase):

    def test_is_reducible(self):
        board = Board()
        board._grid = [ [ 8, 4, 2, 4, ]  # second elt
                      , [ 2, 4, 8, 2, ]  # second elt
                      , [ 4, 2, 4, 8, ]
                      , [ 2, 8, 2, 4, ]
                      , ]

        self.assertTrue(board._is_reducible())

    def test_is_reducible__only_because_of_the_zero(self):
        board = Board()
        board._grid = [ [ 8, 4, 2, 4, ]
                      , [ 2, 0, 4, 2, ]  # second elt
                      , [ 4, 2, 8, 4, ]
                      , [ 2, 8, 4, 2, ]
                      , ]

        self.assertTrue(board._is_reducible())

    def test_not_is_reducible(self):
        board = Board()
        board._grid = [ [ 8, 4, 2, 4, ]
                      , [ 2, 8, 4, 2, ]
                      , [ 4, 2, 8, 4, ]
                      , [ 2, 8, 4, 2, ]
                      , ]

        self.assertFalse(board._is_reducible())
