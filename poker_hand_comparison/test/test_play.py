import unittest

from src.play import compare_hand_strings
from src.hand import Hand


HANDSTRING_GRADE_DICT = {
      'JC KC QC AC TC' : Hand.ROYAL_FLUSH
    , '9H TH 8H 7H JH' : Hand.STRAIGHT_FLUSH
    , 'AS TS 8S 3S JS' : Hand.FLUSH
    , '4H 5D 2S AH 3D' : Hand.STRAIGHT
    , '5S 5C 3H 5D 5H' : Hand.FOUR_OF_A_KIND
    , 'QS KC QD QH KH' : Hand.FULL_HOUSE
    , 'TS 7C TD TH KH' : Hand.THREE_OF_A_KIND
    , 'TS 7C 9D TH 7H' : Hand.TWO_PAIR
    , '6C 7D 8D TH 6H' : Hand.PAIR
    , '6C AS QD TH 5H' : Hand.MEH
    , }


class PlayTest(unittest.TestCase):

    def test_handstring_grade_dict(self):
        for (hand_string, grade) in HANDSTRING_GRADE_DICT.items():
            self.assertEqual(
                Hand.from_string(hand_string).grade(), grade
            )

    def test_compare_hand_strings(self):
        for (hand_string, grade) in HANDSTRING_GRADE_DICT.items():
            for (other_hand_string, other_grade) in HANDSTRING_GRADE_DICT.items():

                if grade > other_grade:
                    self.assertEqual(
                        compare_hand_strings(
                            hand_string,
                            other_hand_string
                        ),
                        Hand.WIN,
                        msg=f'Handstring \'{hand_string}\' of grade <{grade}> DID NOT WIN Handstring \'{other_hand_string}\' of grade <{other_grade}> as was expected!'
                    )

                elif grade < other_grade:
                    self.assertEqual(
                        compare_hand_strings(
                            hand_string,
                            other_hand_string
                        ),
                        Hand.LOSS,
                        msg=f'Handstring \'{hand_string}\' of grade <{grade}> DID NOT incur a LOSS against Handstring \'{other_hand_string}\' of grade <{other_grade}> as was expected!'
                    )

                else:
                    self.assertEqual(
                        compare_hand_strings(
                            hand_string,
                            other_hand_string
                        ),
                        Hand.TIE,
                        msg=f'Handstring \'{hand_string}\' of grade <{grade}> DID NOT TIE up with Handstring \'{other_hand_string}\' of grade <{other_grade}> as was expected!'
                    )


if __name__ == '__main__':
    unittest.main()
