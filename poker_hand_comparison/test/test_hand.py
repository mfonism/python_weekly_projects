import unittest

from src.card import Card
from src.hand import Hand
from src.exceptions import ( InvalidRankError
                           , InvalidSuitError
                           , NonUniqueCardInHandError
                           , InvalidTieBreakError
                           , )


class HandTest(unittest.TestCase):

    def test_hand_cannot_be_instantiated_with_non_unique_card_argument(self):
        card = Card('J', 'C')
        card_clone = Card('J', 'C')
        with self.assertRaises(NonUniqueCardInHandError):
            Hand(card, card_clone, Card('J', 'H'), Card('A', 'D'), Card('3', 'S'))

    def test_hand_equality(self):
        self.assertEqual(
            Hand(Card('K', 'D'), Card('5', 'S'), Card('2', 'H'), Card('A', 'H'), Card('7', 'H')),
            Hand(Card('K', 'D'), Card('5', 'S'), Card('2', 'H'), Card('A', 'H'), Card('7', 'H')))

    def test_hand_equality_takes_ranks_and_suits_of_contained_cards_into_consideration(self):
        # cards are equal when their suits are equal
        # regardless of their ranks
        # but when comparing hands
        # both suit and rank of the contained cards should count
        self.assertNotEqual(
            Hand(Card('K', 'D'), Card('5', 'D'), Card('2', 'H'), Card('A', 'H'), Card('7', 'H')),
            Hand(Card('K', 'D'), Card('5', 'S'), Card('2', 'H'), Card('A', 'H'), Card('7', 'H')))


class HandInstantiationFromStringTest(unittest.TestCase):

    def test_hand_from_string(self):
        self.assertEqual(
            Hand.from_string("KD 5S 2H AH 7H"),
            Hand(Card('K', 'D'), Card('5', 'S'), Card('2', 'H'), Card('A', 'H'), Card('7', 'H')))

    def test_hand_from_string_with_leading_spaces(self):
        self.assertEqual(
            Hand.from_string("      JC KC QC AC TC"),
            Hand(Card('J', 'C'), Card('K', 'C'), Card('Q', 'C'), Card('A', 'C'), Card('T', 'C')))

    def test_hand_from_string_with_trailing_spaces(self):
        self.assertEqual(
            Hand.from_string("4H 5H 2C AS 3D               "),
            Hand(Card('4', 'H'), Card('5', 'H'), Card('2', 'C'), Card('A', 'S'), Card('3', 'D')))

    def test_hand_from_string_with_large_spaces_between_card_specs(self):
        self.assertEqual(
            Hand.from_string("2S   TH 8H  2C             8D"),
            Hand(Card('2', 'S'), Card('T', 'H'), Card('8', 'H'), Card('2', 'C'), Card('8', 'D')))

    def test_hand_from_lower_case_string(self):
        self.assertEqual(
            Hand.from_string("  jc    kc qc ac tc  "),
        Hand(Card('J', 'C'), Card('K', 'C'), Card('Q', 'C'), Card('A', 'C'), Card('T', 'C')))

    def test_hand_from_mixed_case_string(self):
        self.assertEqual(
            Hand.from_string("  Jc kC    Qc      Ac   tc  "),
            Hand(Card('J', 'C'), Card('K', 'C'), Card('Q', 'C'), Card('A', 'C'), Card('T', 'C')))


class HandExpensiveHelperCacheTest(unittest.TestCase):

    def test_sorted_rank_string_is_cached_on_hand_instantiation(self):
        hand = Hand(Card('K', 'D'),
                    Card('5', 'S'),
                    Card('2', 'H'),
                    Card('A', 'H'),
                    Card('7', 'H'))
        self.assertEqual(hand._sorted_rank_string, 'AK752')

    def test_rank_histogram_is_cached_on_hand_instantiation(self):
        hand = Hand(Card('K', 'C'),
                    Card('A', 'S'),
                    Card('K', 'H'),
                    Card('A', 'H'),
                    Card('7', 'H'))
        self.assertDictEqual(hand._rank_histogram,
                             { 'K':2, '7':1, 'A':2, })

    def test_sorted_rank_count_tuple_is_cached_on_hand_instantiation(self):
        hand = Hand(Card('T', 'C'),
                    Card('A', 'S'),
                    Card('T', 'H'),
                    Card('K', 'H'),
                    Card('2', 'H'))
        self.assertListEqual(hand._sorted_rank_count_tuple,
                           [ ('T', 2), ('A', 1), ('K', 1), ('2', 1), ])


class HandEvaluatorsTest(unittest.TestCase):

    def test_is_royal_flush(self):
        cards = [ Card('J', 'C')
                , Card('K', 'C')
                , Card('Q', 'C')
                , Card('A', 'C')
                , Card('T', 'C')
                , ]
        self.assertTrue(Hand(*cards).is_royal_flush())

    def test_not_royal_flush_if_not_flush(self):
        cards = [ Card('J', 'S')
                , Card('K', 'C')
                , Card('Q', 'C')
                , Card('A', 'C')
                , Card('T', 'C')
                , ]
        self.assertFalse(Hand(*cards).is_royal_flush())

    def test_is_straight_flush(self):
        cards = [ Card('J', 'S')
                , Card('9', 'S')
                , Card('7', 'S')
                , Card('8', 'S')
                , Card('T', 'S')
                , ]
        self.assertTrue(Hand(*cards).is_straight_flush())

    def test_not_straight_flush_if_not_flush(self):
        cards = [ Card('J', 'S')
                , Card('9', 'S')
                , Card('7', 'D')
                , Card('8', 'S')
                , Card('T', 'S')
                , ]
        self.assertFalse(Hand(*cards).is_straight_flush())

    def test_not_straight_flush_if_not_straight(self):
        cards = [ Card('J', 'S')
                , Card('9', 'S')
                , Card('2', 'S')
                , Card('8', 'S')
                , Card('T', 'S')
                , ]
        self.assertFalse(Hand(*cards).is_straight_flush())

    def test_is_straight_flush__containing_ace_and_2(self):
        cards = [ Card('A', 'D')
                , Card('3', 'D')
                , Card('2', 'D')
                , Card('4', 'D')
                , Card('5', 'D')
                , ]
        self.assertTrue(Hand(*cards).is_straight_flush())        

    def test_is_flush(self):
        cards = [ Card('9', 'H')
                , Card('T', 'H')
                , Card('2', 'H')
                , Card('4', 'H')
                , Card('K', 'H')
                , ]
        self.assertTrue(Hand(*cards).is_flush())

    def test_is_straight(self):
        cards = [ Card('T', 'H')
                , Card('8', 'C')
                , Card('9', 'H')
                , Card('J', 'H')
                , Card('7', 'H')
                , ]
        self.assertTrue(Hand(*cards).is_straight())

    def test_is_straight__leading_ace(self):
        cards = [ Card('J', 'C')
                , Card('K', 'H')
                , Card('Q', 'D')
                , Card('A', 'S')
                , Card('T', 'D')
                , ]
        self.assertTrue(Hand(*cards).is_straight())

    def test_is_straight__trailing_ace(self):
        cards = [ Card('4', 'H')
                , Card('5', 'H')
                , Card('2', 'C')
                , Card('A', 'H')
                , Card('3', 'D')
                , ]
        self.assertTrue(Hand(*cards).is_straight())

    def test_is_four_of_a_kind(self):
        cards = [ Card('K', 'S')
                , Card('K', 'C')
                , Card('Q', 'D')
                , Card('K', 'D')
                , Card('K', 'H')
                , ]
        self.assertTrue(Hand(*cards).is_four_of_a_kind())

    def test_is_full_house(self):
        cards = [ Card('K', 'S')
                , Card('K', 'C')
                , Card('Q', 'D')
                , Card('Q', 'H')
                , Card('K', 'H')
                , ]
        self.assertTrue(Hand(*cards).is_full_house())

    def test_is_three_of_a_kind(self):
        cards = [ Card('2', 'S')
                , Card('2', 'C')
                , Card('Q', 'D')
                , Card('2', 'H')
                , Card('K', 'H')
                , ]
        self.assertTrue(Hand(*cards).is_three_of_a_kind())

    def test_is_two_pair(self):
        cards = [ Card('2', 'S')
                , Card('T', 'H')
                , Card('8', 'H')
                , Card('2', 'C')
                , Card('8', 'D')
                , ]
        self.assertTrue(Hand(*cards).is_two_pair())

    def test_is_pair(self):    
        cards = [ Card('2', 'S')
                , Card('K', 'C')
                , Card('8', 'D')
                , Card('T', 'H')
                , Card('K', 'H')
                , ]
        self.assertTrue(Hand(*cards).is_pair())


class HandGradeTest(unittest.TestCase):

    def test_grade__royal_flush(self):
        cards = [ Card('J', 'C')
                , Card('K', 'C')
                , Card('Q', 'C')
                , Card('A', 'C')
                , Card('T', 'C')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.ROYAL_FLUSH)

    def test_grade__straight_flush(self):
        cards = [ Card('9', 'H')
                , Card('T', 'H')
                , Card('8', 'H')
                , Card('7', 'H')
                , Card('J', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.STRAIGHT_FLUSH)

    def test_grade__flush(self):
        cards = [ Card('A', 'S')
                , Card('T', 'S')
                , Card('8', 'S')
                , Card('3', 'S')
                , Card('J', 'S')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.FLUSH)

    def test_grade__straight(self):
        cards = [ Card('4', 'H')
                , Card('5', 'D')
                , Card('2', 'S')
                , Card('A', 'H')
                , Card('3', 'D')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.STRAIGHT)

    def test_grade__four_of_a_kind(self):
        cards = [ Card('5', 'S')
                , Card('5', 'C')
                , Card('3', 'H')
                , Card('5', 'D')
                , Card('5', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.FOUR_OF_A_KIND)

    def test_grade__full_house(self):
        cards = [ Card('Q', 'S')
                , Card('K', 'C')
                , Card('Q', 'D')
                , Card('Q', 'H')
                , Card('K', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.FULL_HOUSE)

    def test_grade__three_of_a_kind(self):
        cards = [ Card('T', 'S')
                , Card('7', 'C')
                , Card('T', 'D')
                , Card('T', 'H')
                , Card('K', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.THREE_OF_A_KIND)

    def test_grade__two_pairs(self):
        cards = [ Card('T', 'S')
                , Card('7', 'C')
                , Card('9', 'D')
                , Card('T', 'H')
                , Card('7', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.TWO_PAIR)

    def test_grade__pair(self):
        cards = [ Card('6', 'C')
                , Card('7', 'D')
                , Card('8', 'D')
                , Card('T', 'H')
                , Card('6', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.PAIR)

    def test_grade__uninteresting_hand(self):
        cards = [ Card('6', 'C')
                , Card('A', 'S')
                , Card('Q', 'D')
                , Card('T', 'H')
                , Card('5', 'H')
                , ]
        self.assertEqual(Hand(*cards).grade(),
                         Hand.MEH)


class HandTieBreakerTest(unittest.TestCase):

    def test_break_tie_with__royal_flush(self):
        rf1 = Hand.from_string('JC KC QC AC TC')
        rf2 = Hand.from_string('KH QH JH TH AH')

        self.assertEqual(rf1.grade(), Hand.ROYAL_FLUSH)
        self.assertEqual(rf2.grade(), Hand.ROYAL_FLUSH)

        self.assertEqual(rf1.break_tie_with(rf2), Hand.TIE)

    def test_break_tie_with__straight_flush(self):
        sf1 = Hand.from_string('KC JC QC 9C TC')
        sf2 = Hand.from_string('9H QH JH TH 8H')

        self.assertEqual(sf1.grade(), Hand.STRAIGHT_FLUSH)
        self.assertEqual(sf2.grade(), Hand.STRAIGHT_FLUSH)

        self.assertEqual(sf1.break_tie_with(sf2), Hand.WIN)

    def test_break_tie_with__flush(self):
        f1 = Hand.from_string('9S 6S 2S TS 5S')
        f2 = Hand.from_string('9H QH 2H TH 8H')

        self.assertEqual(f1.grade(), Hand.FLUSH)
        self.assertEqual(f2.grade(), Hand.FLUSH)

        self.assertEqual(f1.break_tie_with(f2), Hand.LOSS)

    def test_break_tie_with__straight(self):
        s1 = Hand.from_string('JC KH QC AD TS')
        s2 = Hand.from_string('4H 5H 2C AD 3H')

        self.assertEqual(s1.grade(), Hand.STRAIGHT)
        self.assertEqual(s2.grade(), Hand.STRAIGHT)

        self.assertEqual(s1.break_tie_with(s2), Hand.WIN)

    def test_break_tie_with__four_of_a_kind(self):
        foak1 = Hand.from_string('TC TH QC TD TS')
        foak2 = Hand.from_string('TH TS TD KC TC')

        self.assertEqual(foak1.grade(), Hand.FOUR_OF_A_KIND)
        self.assertEqual(foak2.grade(), Hand.FOUR_OF_A_KIND)

        self.assertEqual(foak1.break_tie_with(foak2), Hand.LOSS)

    def test_break_tie_with__full_house(self):
        fh1 = Hand.from_string('KC KH 4C 4D KS')
        fh2 = Hand.from_string('5H 5D KC KD KS')

        self.assertEqual(fh1.grade(), Hand.FULL_HOUSE)
        self.assertEqual(fh2.grade(), Hand.FULL_HOUSE)

        self.assertEqual(fh1.break_tie_with(fh2), Hand.LOSS)

    def test_break_tie_with__three_of_a_kind(self):
        toak1 = Hand.from_string('2C 2H 4C 2D KS')
        toak2 = Hand.from_string('2H 3H 2C KD 2S')

        self.assertEqual(toak1.grade(), Hand.THREE_OF_A_KIND)
        self.assertEqual(toak2.grade(), Hand.THREE_OF_A_KIND)

        self.assertEqual(toak1.break_tie_with(toak2), Hand.WIN)

    def test_break_tie_with__two_pair(self):
        tp1 = Hand.from_string('2C TH 8C 2D 8S')
        tp2 = Hand.from_string('8H 8S 5C 5D TH')

        self.assertEqual(tp1.grade(), Hand.TWO_PAIR)
        self.assertEqual(tp2.grade(), Hand.TWO_PAIR)

        self.assertEqual(tp1.break_tie_with(tp2), Hand.LOSS)

    def test_break_tie_with__pair(self):
        p1 = Hand.from_string('2C TH TC 4D 5S')
        p2 = Hand.from_string('4H 3S TC 2D TH')

        self.assertEqual(p1.grade(), Hand.PAIR)
        self.assertEqual(p2.grade(), Hand.PAIR)

        self.assertEqual(p1.break_tie_with(p2), Hand.WIN)

    def test_break_tie_with__uninteresting_hand(self):
        uh1 = Hand.from_string('6C AS QD TH 5H')
        uh2 = Hand.from_string('6C 3S QD TH 5H')

        self.assertEqual(uh1.grade(), Hand.MEH)
        self.assertEqual(uh2.grade(), Hand.MEH)

        self.assertEqual(uh1.break_tie_with(uh2), Hand.WIN)

    def test_cannot_break_tie_for_unequally_graded_hands(self):
        HANDSTRING_GRADE_DICT = {
              'JC KC QC AC TC' : Hand.ROYAL_FLUSH
            , '9H TH 8H 7H JH' : Hand.STRAIGHT_FLUSH
            , 'AS TS 8S 3S JS' : Hand.FLUSH
            , '4H 5D 2S AH 3D' : Hand.STRAIGHT
            , '5S 5C 3H 5D 5H' : Hand.FOUR_OF_A_KIND
            , 'QS KC QD QH KH' : Hand.FULL_HOUSE
            , 'TS 7C TD TH KH' : Hand.THREE_OF_A_KIND
            , 'TS 7C 9D TH 7H' : Hand.TWO_PAIR
            , '6C 7D 8D TH 6H' : Hand.PAIR
            , '6C AS QD TH 5H' : Hand.MEH
            , }

        for (hand_string, grade) in HANDSTRING_GRADE_DICT.items():
            self.assertEqual(Hand.from_string(hand_string).grade(),
                             grade)

        for hand_string in HANDSTRING_GRADE_DICT:
            for other_hand_string in HANDSTRING_GRADE_DICT.keys() - {hand_string}:
                self.assertNotEqual(hand_string, other_hand_string)
                with self.assertRaises(InvalidTieBreakError):
                    Hand.from_string(hand_string).break_tie_with(Hand.from_string(other_hand_string))


if __name__ == '__main__':
    unittest.main()
