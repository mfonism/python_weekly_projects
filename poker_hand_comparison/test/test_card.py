import unittest

from src.card import Card
from src.exceptions import ( InvalidRankError
                           , InvalidSuitError
                           , InvalidCardStringError
                           , )


class CardTest(unittest.TestCase):

    def test_card_cannot_be_instantiated_with_invalid_rank(self):
        self.assertTrue('J' in Card.VALID_RANKS)
        self.assertFalse('0' in Card.VALID_RANKS)

        Card('J', 'C')
        with self.assertRaises(InvalidRankError):
            Card('0', 'C')

    def test_card_cannot_be_instantiated_with_invalid_suit(self):
        self.assertTrue('C' in Card.VALID_SUITS)
        self.assertFalse('Z' in Card.VALID_SUITS)

        Card('J', 'C')
        with self.assertRaises(InvalidSuitError):
            Card('J', 'Z')

    def test_card_may_be_instantiated_with_lower_case_suit_and_rank(self):
        # so long as their upper case variants
        # are valid
        lower_card = Card('j', 'c')
        self.assertEqual(lower_card.rank, 'J')
        self.assertEqual(lower_card.suit, 'C')

    def test_cards_of_same_rank_are_equal_regardless_of_suit(self):
        # same rank, same suit
        self.assertEqual(Card('K', 'S'), Card('K', 'S'))
        # same rank, different suits
        self.assertEqual(Card('K', 'S'), Card('K', 'H'))

    def test_cards_of_different_ranks_are_not_equal(self):
        # different ranks, different suits
        self.assertNotEqual(Card('K', 'S'), Card('T', 'H'))
        # different ranks, same suit
        self.assertNotEqual(Card('K', 'S'), Card('T', 'S'))

    def test_card_of_higher_rank_is_greater_than_card_of_lower_rank(self):
        self.assertGreater(Card('K', 'S'), Card('T','D'))
        self.assertGreater(Card('K', 'D'), Card('T','S'))

    def test_card_of_lower_rank_is_less_than_card_of_higher_rank(self):
        self.assertLess(Card('T', 'S'), Card('K','D'))
        self.assertLess(Card('T', 'D'), Card('K','S'))

    def test_different_card_objects_of_equal_rank_and_equal_suit_hash_to_the_same_value(self):
        card1 = Card('K', 'S')
        card2 = Card('K', 'S')
        self.assertEqual(hash(card1), hash(card2))

    def test_card_objects_of_unequal_rank_or_unequal_suit_hash_to_different_values(self):
        # unequal rank, unequal suit
        self.assertNotEqual(hash(Card('Q', 'C')), hash(Card('9', 'D')))
        # unequal rank, equal suit
        self.assertNotEqual(hash(Card('Q', 'D')), hash(Card('9', 'D')))
        # equal rank, unequal suit
        self.assertNotEqual(hash(Card('Q', 'C')), hash(Card('Q', 'D')))


class CardInstantiationFromStringTest(unittest.TestCase):

    def test_card_from_string(self):
        self.assertEqual(Card.from_string('2H'), Card('2', 'H'))

    def test_cannot_instantiate_card_from_string_of_less_than_two_chars(self):
        with self.assertRaises(InvalidCardStringError):
            Card.from_string('')
        with self.assertRaises(InvalidCardStringError):
            Card.from_string('2')
        with self.assertRaises(InvalidCardStringError):
            Card.from_string('H')

    def test_cannot_instantiate_card_from_string_of_greater_than_two_chars(self):
        with self.assertRaises(InvalidCardStringError):
            Card.from_string('2H ')
        with self.assertRaises(InvalidCardStringError):
            Card.from_string(' 2H')
        with self.assertRaises(InvalidCardStringError):
            Card.from_string('2 H')


if __name__ == '__main__':
    unittest.main()
