# Poker Hand Comparison

A program to compare two hands of poker according to [the hand evaluation rules of Texas Hold'em](https://pokerstrategy.com/poker-hand-charts-evaluations/).

Made with Python.

## Setup Instructions

Clone the repository

    $ git clone https://bitbucket.org/mfonism/python_weekly_projects.git

This will download the python_weekly_projects repository, containing this project amongst other projects, to your computer.

cd into this project's directory in the local repository

    $ cd python_weekly_projects/poker_hand_comparison/

At this point you will need to have python available on your command line to be able to proceed.

run the tests

    $ python -m unittest discover

Invoke the python shell

    $ python

In the shell, import the `compare_hand_strings` function from the src package

    >>> from src import compare_hand_strings

Play with it

    >>> compare_hand_strings('TC TH QC TD TS', 'TH TS TD KC TC')

That should return `2`, representing a loss for the first argument hand string. (`1` is for a win and `3` is for a tie).

## Reflection

This was a week long personal project aimed at helping me improve my Test-Driven Development abilities. Matter of fact, it was my first attempt at a Python project since learning TDD (I learnt TDD from the Pharo dialect of Smalltalk).

It was a fantastic learning experience.

I learnt how to organize my code into modules and packages. I learnt test discovery in command line python. And I learnt patience and self-restraint - that one's on TDD!
