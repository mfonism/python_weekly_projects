# Connect-n Checker

A Python program to check whether there is an n-length line of connected cells marked by a certain player in a game of **connect-n**.

## Setup Instructions

    $ git clone https://bitbucket.org/mfonism/python_weekly_projects.git

This will download the parent repository to your computer.

cd into this project's directory.

    $ cd python_weekly_projects/connect_n_checker/

run the tests

    $ python -m unittest discover

Invoke the python shell

    $ python

In the shell import the classes `Cell` and `Hand` from the src package

    >>> from src import Cell, Hand

Instantiate two hand objects to represent two players

    >>> x_player = Hand()
    >>> o_player = Hand()

Now, let's assume we're playing connect 3 (also known as tic-tac-toe)

    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+

Everytime a player selects a cell on the 3x3 board, add the respective cell to the player's hand.

**x_player's turn**

    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+
    |     |  X  |     |
    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+


    >>> x_player.add_cell(Cell(1,1))

Then get the list of length-3 connections made by that particular move for that player

    >>> x_player.connect_n_through_cell(3, Cell(1,1))
    []

An empty list is returned showing that no connections of length-3 were made by that move

**o_player's turn**

    +-----+-----+-----+
    |     |     |  O  |
    +-----+-----+-----+
    |     |  X  |     |
    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+


    >>> o_player.add_cell(Cell(0,2))
    >>> o_player.connect_n_through_cell(3, Cell(0,2))
    []

**x_player's turn**

    +-----+-----+-----+
    |  X  |     |  O  |
    +-----+-----+-----+
    |     |  X  |     |
    +-----+-----+-----+
    |     |     |     |
    +-----+-----+-----+


    >>> x_player.add_cell(Cell(0,0))
    >>> x_player.connect_n_through_cell(3, Cell(0,0))
    []

**o_player's turn**

    +-----+-----+-----+
    |  X  |     |  O  |
    +-----+-----+-----+
    |     |  X  |     |
    +-----+-----+-----+
    |     |     |  O  |
    +-----+-----+-----+


    >>> o_player.add_cell(Cell(2,2))
    >>> o_player.connect_n_through_cell(3, Cell(2,2))
    []

**x_player's turn**

    +-----+-----+-----+
    |  X  |     |  O  |
    +-----+-----+-----+
    |     |  X  |     |
    +-----+-----+-----+
    |  X  |     |  O  |
    +-----+-----+-----+


    >>> x_player.add_cell(Cell(2,0))  # an admittedly thoughtless move
    >>> x_player.connect_n_through_cell(3, Cell(2,0))
    []

**o_player's turn**

    +-----+-----+-----+
    |  X  |     |  O  |
    +-----+-----+-----+
    |     |  X  |  O  |
    +-----+-----+-----+
    |  X  |     |  O  |
    +-----+-----+-----+


    >>> o_player.add_cell(Cell(1,2))
    >>> o_player.connect_n_through_cell(3, Cell(1,2))
    [ColumnConnection(_from=Cell(x=0, y=2), to=Cell(x=2, y=2))]

The list returned contains a `ColumnConnection` object, which tells us **o_player** has created a column of three connected cells by that last move.

The column of connected cells spans from Cell(0,2) to Cell(2,2).

## Reflection

This was the fourth consecutive project where I had to deal with grids.

It was the third consecutive project where I did not implement my grid using a list of lists (as is conventional). Instead, I concerned myself with only the *important* cells in the grid. This way, I did not have to bother about the size of the grid.

**And now for something new:**

I came across semantic versioning online, and decided to use it starting from this project.

I have noticed that my commit messages are more suggestive of what the respective commits were going to do, and where they were going to do them.

**Message To My Future (read: next week) Self**

Hello, @mfonism, this is you from last week.

You need to start aliasing your git commands.
