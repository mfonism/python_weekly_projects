import unittest

from src.cell import Cell


class CellTest(unittest.TestCase):
    '''
        (1,1)   (1,2)   (1,3)
        (2,1)     X     (2,3)
        (3,1)   (3,2)   (3,3)
    '''

    def test_get_top_neighbor(self):
        self.assertEqual(Cell(2,2).get_top_neighbor(), Cell(1,2))

    def test_get_right_neighbor(self):
        self.assertEqual(Cell(2,2).get_right_neighbor(), Cell(2,3))

    def test_get_bottom_neighbor(self):
        self.assertEqual(Cell(2,2).get_bottom_neighbor(), Cell(3,2))

    def test_get_left_neighbor(self):
        self.assertEqual(Cell(2,2).get_left_neighbor(), Cell(2,1))

    def test_get_top_right_neighbor(self):
        self.assertEqual(Cell(2,2).get_top_right_neighbor(), Cell(1,3))

    def test_get_bottom_right_neighbor(self):
        self.assertEqual(Cell(2,2).get_bottom_right_neighbor(), Cell(3,3))

    def test_get_bottom_left_neighbor(self):
        self.assertEqual(Cell(2,2).get_bottom_left_neighbor(), Cell(3,1))

    def test_get_top_left_neighbor(self):
        self.assertEqual(Cell(2,2).get_top_left_neighbor(), Cell(1,1))


if __name__ == '__main__':
    unittest.main()
