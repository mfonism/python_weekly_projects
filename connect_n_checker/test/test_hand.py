import unittest

from src.hand import Hand
from src.cell import Cell
from src.connection import Connection, RowConnection, ColumnConnection, FwdSlashConnection, BckSlashConnection


class HandTest(unittest.TestCase):

    def setUp(self):
        #                             (0,4), (0,5)
        # (1,0), (1,1),        (1,3),
        # (2,0),        (2,2), (2,3), (2,4),
        #        (3,1), (3,2), (3,3),
        #        (4,1),        (4,3)

        coords = [
                                        (0,4), (0,5),
            (1,0), (1,1),        (1,3),
            (2,0),        (2,2), (2,3), (2,4),
                   (3,1), (3,2), (3,3),
                   (4,1),        (4,3),
            ]

        hand = Hand()
        for (i,j) in coords:
            hand.add_cell(Cell(i,j))

        self.hand = hand

    def test_get_connections_through_cell(self):
        self.assertCountEqual(
            self.hand.get_connections_through_cell(Cell(3,3)),
            [ RowConnection(_from=Cell(3,1), to=Cell(3,3))
            , ColumnConnection(_from=Cell(1,3), to=Cell(4,3))
            , FwdSlashConnection(_from=Cell(3,3), to=Cell(2,4))
            , BckSlashConnection(_from=Cell(1,1), to=Cell(3,3))
            , ]
            )

    def test_get_connections_through_cell__alt(self):
        hand = Hand()
        for (i,j) in [ (1,0), (1,1),
                       (2,0),        ]:
            hand.add_cell(Cell(i,j))

        self.assertCountEqual(
            hand.get_connections_through_cell(Cell(1,1)),
            [ RowConnection(_from=Cell(1,0), to=Cell(1,1))
            , FwdSlashConnection(_from=Cell(2,0), to=Cell(1,1))
            , ]
            )

    def test_get_row_connection_through_cell(self):
        self.assertEqual(
            self.hand.get_row_connection_through_cell(Cell(3,3)),
            RowConnection(_from=Cell(3,1), to=Cell(3,3))
            )

        self.assertEqual(
            self.hand.get_row_connection_through_cell(Cell(2,3)),
            RowConnection(_from=Cell(2,2), to=Cell(2,4))
            )

    def test_get_leftmost_connected_cell_on_same_row(self):
        self.assertEqual(
            self.hand.get_leftmost_connected_cell_on_same_row(Cell(3,3)),
            Cell(3,1)
            )

        self.assertEqual(
            self.hand.get_leftmost_connected_cell_on_same_row(Cell(2,3)),
            Cell(2,2)
            )

    def test_get_rightmost_connected_cell_on_same_row(self):
        self.assertEqual(
            self.hand.get_rightmost_connected_cell_on_same_row(Cell(3,3)),
            Cell(3,3)
            )

        self.assertEqual(
            self.hand.get_rightmost_connected_cell_on_same_row(Cell(2,3)),
            Cell(2,4)
            )

    def test_get_column_connection_through_cell(self):
        self.assertEqual(
            self.hand.get_column_connection_through_cell(Cell(3,3)),
            ColumnConnection(_from=Cell(1,3), to=Cell(4,3))
            )

        self.assertEqual(
            self.hand.get_column_connection_through_cell(Cell(1,0)),
            ColumnConnection(_from=Cell(1,0), to=Cell(2,0))
            )

    def test_get_topmost_connected_cell_on_same_column(self):
        self.assertEqual(
            self.hand.get_topmost_connected_cell_on_same_column(Cell(3,3)),
            Cell(1,3)
            )

        self.assertEqual(
            self.hand.get_topmost_connected_cell_on_same_column(Cell(3,1)),
            Cell(3,1)
            )

    def test_get_bottommost_connected_cell_on_same_column(self):
        self.assertEqual(
            self.hand.get_bottommost_connected_cell_on_same_column(Cell(3,3)),
            Cell(4,3)
            )

        self.assertEqual(
            self.hand.get_bottommost_connected_cell_on_same_column(Cell(3,1)),
            Cell(4,1)
            )

    def test_get_fwd_slash_connection_through_cell(self):
        self.assertEqual(
            self.hand.get_fwd_slash_connection_through_cell(Cell(3,3)),
            FwdSlashConnection(_from=Cell(3,3), to=Cell(2,4))
            )

        self.assertEqual(
            self.hand.get_fwd_slash_connection_through_cell(Cell(2,2)),
            FwdSlashConnection(_from=Cell(3,1), to=Cell(0,4))
            )

    def test_get_topmost_connected_cell_on_same_fwd_slash(self):
        self.assertEqual(
            self.hand.get_topmost_connected_cell_on_same_fwd_slash(Cell(3,3)),
            Cell(2,4)
            )

        self.assertEqual(
            self.hand.get_topmost_connected_cell_on_same_fwd_slash(Cell(2,2)),
            Cell(0,4)
            )

    def test_get_bottommost_connected_cell_on_same_fwd_slash(self):
        self.assertEqual(
            self.hand.get_bottommost_connected_cell_on_same_fwd_slash(Cell(3,3)),
            Cell(3,3)
            )

        self.assertEqual(
            self.hand.get_bottommost_connected_cell_on_same_fwd_slash(Cell(2,2)),
            Cell(3,1)
            )

    def test_get_bck_slash_connection_through_cell(self):
        self.assertEqual(
            self.hand.get_bck_slash_connection_through_cell(Cell(3,3)),
            BckSlashConnection(_from=Cell(1,1), to=Cell(3,3))
            )

        self.assertEqual(
            self.hand.get_bck_slash_connection_through_cell(Cell(3,2)),
            BckSlashConnection(_from=Cell(3,2), to=Cell(4,3))
            )

    def test_get_topmost_connected_cell_on_same_bck_slash(self):
        self.assertEqual(
            self.hand.get_topmost_connected_cell_on_same_bck_slash(Cell(3,3)),
            Cell(1,1)
            )

        self.assertEqual(
            self.hand.get_topmost_connected_cell_on_same_bck_slash(Cell(3,2)),
            Cell(3,2)
            )

    def test_get_bottommost_connected_cell_on_same_bck_slash(self):
        self.assertEqual(
            self.hand.get_bottommost_connected_cell_on_same_bck_slash(Cell(3,3)),
            Cell(3,3)
            )

        self.assertEqual(
            self.hand.get_bottommost_connected_cell_on_same_bck_slash(Cell(3,2)),
            Cell(4,3)
            )


class HandConnectNTest(unittest.TestCase):

    def test_connect_3(self):
        hand = Hand()
        for coord in [
            (0,0),        (0,2),
                   (1,1),
                          (2,2),
            ]:
            hand.add_cell(Cell(*coord))

        self.assertEqual(
            hand.connect_n_through_cell(3, Cell(1,1)),
            [ BckSlashConnection(_from=Cell(0,0), to=Cell(2,2)), ]
            )

    def test_cant_connect_3(self):
        hand = Hand()
        for coord in [
            (0,0),        (0,2),

                          (2,2),
            ]:
            hand.add_cell(Cell(*coord))

        self.assertEqual(
            hand.connect_n_through_cell(3, Cell(0,2)),
            [ ]
            )


if __name__ == '__main__':
    unittest.main()
