import unittest

from src.connection import Connection, RowConnection, ColumnConnection, FwdSlashConnection, BckSlashConnection
from src.exceptions import InvalidConnectionError
from src.cell import Cell


class ConnectionTest(unittest.TestCase):

    def test_row_connection(self):
        # (3,1), (3,2), (3,3), (3,4), (3,5)
        conn = Connection.create_connection(_from=Cell(3,1), to=Cell(3,5))

        self.assertIsInstance(conn, RowConnection)
        self.assertEqual(len(conn), 5)

    def test_column_connection(self):
        # (0,4), (1,4), (2,4)
        conn = Connection.create_connection(_from=Cell(0,4), to=Cell(2,4))

        self.assertIsInstance(conn, ColumnConnection)
        self.assertEqual(len(conn), 3)

    def test_fwd_slash_connection(self):
        # (3,1), (2,2), (1,3) (0,4)
        conn = Connection.create_connection(_from=Cell(3,1), to=Cell(0,4))

        self.assertIsInstance(conn, FwdSlashConnection)
        self.assertEqual(len(conn), 4)

    def test_bck_slash_connection(self):
        # (2,0), (3,1)
        conn = Connection.create_connection(_from=Cell(2,0), to=Cell(3,1))

        self.assertIsInstance(conn, BckSlashConnection)
        self.assertEqual(len(conn), 2)

    def test_not_a_connection(self):
        with self.assertRaises(InvalidConnectionError):
            conn = Connection.create_connection(_from=Cell(0,0), to=Cell(1,2))

    def test_single_cell_connection_cant_be_created(self):
        # (1,0)
        conn = Connection.create_connection(_from=Cell(1,0), to=Cell(1,0))
        self.assertIsNone(conn)


if __name__ == '__main__':
    unittest.main()
