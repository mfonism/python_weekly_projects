class Cell:
    '''A cell has an x coordinate and a y coordinate.'''

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_top_neighbor(self):
        return Cell(self.x - 1, self.y)

    def get_right_neighbor(self):
        return Cell(self.x, self.y + 1)

    def get_bottom_neighbor(self):
        return Cell(self.x + 1, self.y)

    def get_left_neighbor(self):
        return Cell(self.x, self.y - 1)

    def get_top_right_neighbor(self):
        return Cell(self.x - 1, self.y + 1)

    def get_bottom_right_neighbor(self):
        return Cell(self.x + 1, self.y + 1)

    def get_bottom_left_neighbor(self):
        return Cell(self.x + 1, self.y - 1)

    def get_top_left_neighbor(self):
        return Cell(self.x - 1, self.y - 1)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return f'Cell(x={self.x}, y={self.y})'

    def __hash__(self):
        return hash(str(self))
