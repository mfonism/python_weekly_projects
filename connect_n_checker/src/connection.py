from .exceptions import InvalidConnectionError


class Connection:
    '''A connection is a straight line from one cell to another.'''

    @staticmethod
    def create_connection(_from, to):
        if _from.x == to.x:
            if _from.y == to.y:
                return None     # single cell connection cannot be created
            return RowConnection(_from, to)
        elif _from.y == to.y:
            return ColumnConnection(_from, to)
        elif _from.x + _from.y == to.x + to.y:
            return FwdSlashConnection(_from, to)
        elif _from.x - to.x == _from.y - to.y:
            return BckSlashConnection(_from, to)
        else:
            raise InvalidConnectionError(f'Cannot create a connection from {_from} to {to}!')

    def __init__(self, _from, to):
        self.starting_cell = _from
        self.ending_cell = to

    def __len__(self):
        return max(
            abs(self.starting_cell.x - self.ending_cell.x),
            abs(self.starting_cell.y - self.ending_cell.y)
            ) + 1

    def __eq__(self, other):
        return { self.starting_cell, self.ending_cell } == { other.starting_cell, other.ending_cell}

    def __hash__(self):
        return hash(''.join(sorted(map(str, [ self.starting_cell, self.ending_cell ]))))

    def __repr__(self):
        return f'{self.get_class_name()}(_from={self.starting_cell}, to={self.ending_cell})'

    def get_class_name(self):
        return 'Connection'


class RowConnection(Connection):
    '''A connection along a row.'''

    def get_class_name(self):
        return 'RowConnection'


class ColumnConnection(Connection):
    '''A connnection along a column.'''

    def get_class_name(self):
        return 'ColumnConnection'


class FwdSlashConnection(Connection):
    '''A connection that forms a forward slash.'''

    def get_class_name(self):
        return 'FwdSlashConnection'


class BckSlashConnection(Connection):
    '''A connection that forms a back slash.'''

    def get_class_name(self):
        return 'BckSlashConnection'
