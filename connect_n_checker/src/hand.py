from .connection import Connection

class Hand:
    '''A hand is a collection of cells that have been marked by a player.'''
    
    def __init__(self):
        self.cells = set()

    def add_cell(self, cell):
        self.cells.add(cell)

    def connect_n_through_cell(self, n, cell):
        '''Returns a list of n-length connections passing through 
        argument cell.'''
        return list(filter(lambda conn: len(conn) == n, self.get_connections_through_cell(cell)))

    def get_connections_through_cell(self, cell):
        '''Returns a list of connections which pass through argument cell.'''
        return list(filter((lambda conn : conn is not None),
               [ self.get_row_connection_through_cell(cell)
               , self.get_column_connection_through_cell(cell)
               , self.get_fwd_slash_connection_through_cell(cell)
               , self.get_bck_slash_connection_through_cell(cell)
               , ]))

    def get_row_connection_through_cell(self, cell):
        '''Returns a row connection which passes through argument cell.'''
        return Connection.create_connection(
            _from=self.get_leftmost_connected_cell_on_same_row(cell),
            to=self.get_rightmost_connected_cell_on_same_row(cell)
            )

    def _step_until(self, start, get_next, can_stop_based_on_next):
        '''Step with a stepping function, from a starting point, 
        until a stopping predicate passes. Then stop.'''
        _next = get_next(start)

        if can_stop_based_on_next(_next):
            return start
        return self._step_until(_next, get_next, can_stop_based_on_next)

    def step_until_out_of_hand(self, cell, stepping_func):
        '''Returns the farthest connected cell to argument cell in 
        the appropriate direction on the appropriate line.

        Args:
            cell:              the current cell.
            stepping_function: the function which dictates the direction 
                in which to step.
            stopping_pred:     a predicate which determines when to stop.
        '''
        return self._step_until(cell, stepping_func, lambda _next: _next not in self.cells)

    def get_leftmost_connected_cell_on_same_row(self, cell):
        '''Returns the leftmost cell connected to argument cell on its row.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_left_neighbor())

    def get_rightmost_connected_cell_on_same_row(self, cell):
        '''Returns the rightmost cell connected to argument cell on its row.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_right_neighbor())

    def get_column_connection_through_cell(self, cell):
        '''Returns a column connection which passes through argument cell.'''
        return Connection.create_connection(
            _from=self.get_topmost_connected_cell_on_same_column(cell),
            to=self.get_bottommost_connected_cell_on_same_column(cell)
            )

    def get_topmost_connected_cell_on_same_column(self, cell):
        '''Returns the topmost cell connected to argument cell 
        on its column.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_top_neighbor())

    def get_bottommost_connected_cell_on_same_column(self, cell):
        '''Returns the bottommost cell connected to argument cell 
        on its column.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_bottom_neighbor())

    def get_fwd_slash_connection_through_cell(self, cell):
        '''Returns a forward slash styled connection which passes through 
        the argument cell.'''
        return Connection.create_connection(
            _from=self.get_topmost_connected_cell_on_same_fwd_slash(cell),
            to=self.get_bottommost_connected_cell_on_same_fwd_slash(cell)
            )

    def get_topmost_connected_cell_on_same_fwd_slash(self, cell):
        '''Returns the topmost cell connected to argument cell 
        on its forward slash diagonal.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_top_right_neighbor())

    def get_bottommost_connected_cell_on_same_fwd_slash(self, cell):
        '''Returns the bottommost cell connected to argument cell 
        on its forward slash diagonal.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_bottom_left_neighbor())

    def get_bck_slash_connection_through_cell(self, cell):
        '''Returns a back slash styled connection which passes through 
        the argument cell.'''
        return Connection.create_connection(
            _from=self.get_topmost_connected_cell_on_same_bck_slash(cell),
            to=self.get_bottommost_connected_cell_on_same_bck_slash(cell)
            )

    def get_topmost_connected_cell_on_same_bck_slash(self, cell):
        '''Returns the topmost cell connected to argument cell 
        on its back slash diagonal.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_top_left_neighbor())

    def get_bottommost_connected_cell_on_same_bck_slash(self, cell):
        '''Returns the bottommost cell connected to argument cell 
        on its back slash diagonal.'''
        return self.step_until_out_of_hand(cell, lambda c : c.get_bottom_right_neighbor())
