from .grid import FiniteGrid


class Universe:
    '''A universe is an infinitely tall and infinitely wide grid.

    The indices of rows and columns in a universe may 
    take any sign, positive or negative.
    Furthermore, each cell in the universe is either 
    DEAD or (exclusive) ALIVE.
    '''

    @staticmethod
    def yield_neighbors_for_cell(i,j):
        '''Returns a generator of the (eight) neighboring 
        cells of the cell indexed by row i and column j.

        Args:
            i: row index of cell in consideration.
            j: column index of cell in consideration.

        Returns:
            generator of (r,c) tuples describing the row 
            indices and the corresponding column indices of 
            the cells surrounding the cell at (i,j).
        '''     

        # return ((r,c) for r in range(i-1, (i+1)+1)
        #               for c in range(j-1, (j+1)+1)
        #               if not (r == i and c == j))
        return ((r,c) for r in range(i-1, i+2)
                      for c in range(j-1, j+2)
                      if r != i or c != j)

    @classmethod
    def yield_next_generation_cells(cls, pattern):
        '''Returns a generator of the pattern of the next 
        generation from the pattern of the current generation.

        Args:
            pattern: a set of (r,c) tuples describing the 
                row indices and the corresponding 
                column indices of living cells in the current 
                evolutionary stage of a universe.

        Returns:
            generator of (r,c) tuples describing the row 
            indices and the corresponding column indices 
            in the next evolutionary stage of the universe.
        '''
        green_histogram = {}    # some living cells may lose life
        amber_histogram = {}    # some dead cells may gain new life

        for life in pattern:
            for neighbor in cls.yield_neighbors_for_cell(*life):
                if neighbor in pattern:
                    # one more living neighbor for live cell
                    green_histogram[life] = green_histogram.get(life, 0) + 1
                else:
                    # live cell got a neighbor that may gain new life
                    amber_histogram[neighbor] = amber_histogram.get(neighbor, 0) + 1

        for (greenie, green_neighbor_count) in green_histogram.items():
            if green_neighbor_count in (2,3):
                # remain amongst the living
                yield greenie

        for (amberlyn, green_neighbor_count) in amber_histogram.items():
            if green_neighbor_count == 3:
                # Lazarus, come forth!
                yield amberlyn

    def __init__(self, init_pattern):
        '''Instantiates a Universe with an initial pattern.

        A pattern is a set of coordinates of living cells.
        '''
        self._pattern = init_pattern

    def tick(self):
        '''Causes this universe to evolve by one step.'''
        self._pattern = list(self.yield_next_generation_cells(self._pattern))

    def to_finite_grid(self):
        return FiniteGrid(self._pattern)
