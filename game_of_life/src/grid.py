import math
import copy


class FiniteGrid:
    '''A finite grid is bounded in height and width.

    The indices of the rows and columns in a finite grid 
    may take any sign, positive or negative.
    '''

    @staticmethod
    def compute_bounding_box(pattern):
        '''Compute the dimensional specification of a 
        box which just encloses all the cells whose 
        indices are specified in the argument pattern.

        Args:
            pattern: an iterable of tuples describing the 
            indices of living cells.

        Returns:
            a four tuple in the manner of
            (row_start, row_span, col_start, col_span)
        '''
        i_min = j_min =  math.inf
        i_max = j_max = -math.inf

        for (i,j) in pattern:
            i_min = min(i, i_min)
            i_max = max(i, i_max)
            j_min = min(j, j_min)
            j_max = max(j, j_max)

        return ( i_min
               , i_max - i_min + 1
               , j_min
               , j_max - j_min + 1
               )
    
    def __init__(self, pattern):
        '''Instantiate a finite grid that just encloses 
        all the cells whose indices are specified in the 
        argument pattern.
        
        Cells whose indices are specified in the argument pattern will be populated with 1s,
        0s will be placed everywhere else.
        '''
        ( self.row_start
        , self.row_span
        , self.col_start
        , self.col_span
        , ) = self.compute_bounding_box(pattern)

        self._grid = [ ([ 0 ] * self.col_span) for row in range(self.row_span) ]

        for (i,j) in pattern:
            self._grid[ i - self.row_start ][ j - self.col_start ] = 1

    def grid(self):
        return copy.deepcopy(self._grid)

    def __str__(self):
        return '\n'.join(
            [ '['
            , ',\n'.join(map(str, (row for row in self._grid)))
            , ']'
            , ])
