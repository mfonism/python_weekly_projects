import unittest

from src.universe import Universe


class UniverseTest(unittest.TestCase):
    '''You may need a piece of paper with a grid on it
    to check that these tests actually test the right thing.

    Thankfully, the coordinates are written as if this file 
    were a grid.
    '''

    def test_yield_neighbors_for_cell(self):
        self.assertEqual(
            list(Universe.yield_neighbors_for_cell(0,0)),
            [ (-1,-1), (-1,0), (-1,1),
              ( 0,-1),         ( 0,1),
              ( 1,-1), ( 1,0), ( 1,1), ]
        )

    def test_yield_next_generation_cells__for_blinker(self):
        a_blinker_config = { (0,0),
                             (1,0),
                             (2,0), }

        self.assertCountEqual(
            list(Universe.yield_next_generation_cells(a_blinker_config)),
            [ (1,-1), (1,0), (1,1), ]
        )

    def test_yield_next_generation_cells__for_toad(self):
        a_toad_config = {        (0,1), (0,2), (0,3),
                          (1,0), (1,1), (1,2),        }

        self.assertCountEqual(
            list(Universe.yield_next_generation_cells(a_toad_config)),
            [          (-1, 2),
              ( 0, 0),          ( 0, 3),
              ( 1, 0),          ( 1, 3),
              ( 2, 1),                   ]
        )

    def test_yield_next_generation_cells__for_beacon(self):
        a_beacon_config = { (-2,-2), (-2,-1),
                            (-1,-2), (-1,-1),
                                              ( 0, 0), ( 0, 1),
                                              ( 1, 0), ( 1, 1), }

        self.assertCountEqual(
            list(Universe.yield_next_generation_cells(a_beacon_config)),
            [ (-2,-2), (-2,-1),
              (-1,-2),
                                         ( 0, 1),
                                ( 1, 0), ( 1, 1), ]
        )


if __name__ == '__main__':
    unittest.main()
