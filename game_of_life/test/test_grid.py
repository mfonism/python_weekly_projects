import unittest

from src.grid import FiniteGrid
from src.universe import Universe


class FiniteGridTest(unittest.TestCase):

    def setUp(self):
        pattern = {          (-2, 0),
                    (-1,-1), (-1, 0), (-1, 1),
                             ( 0, 0),
                    ( 1,-1), ( 1, 0), ( 1, 1),
                    ( 2,-1),          ( 2, 1), }
        self.fg = FiniteGrid(pattern)

    def test_fg_dimensions(self):
        self.assertEqual(self.fg.row_start, -2)
        self.assertEqual(self.fg.row_span, 5)

        self.assertEqual(self.fg.col_start, -1)
        self.assertEqual(self.fg.col_span, 3)

    def test_subgrid_grid(self):
        self.assertListEqual(self.fg.grid(),
            [ [ 0, 1, 0, ]
            , [ 1, 1, 1, ]
            , [ 0, 1, 0, ]
            , [ 1, 1, 1, ]
            , [ 1, 0, 1, ]
            ])


class UniverseToFiniteGridTest(unittest.TestCase):

    def test_beacon_as_finite_grid(self):
        config = { (-2,-4), (-2,-3),
                   (-1,-4), (-1,-3),
                                     ( 0,-2), ( 0,-1),
                                     ( 1,-2), ( 1,-1), }
        uni = Universe(config)
        self.assertListEqual(uni.to_finite_grid().grid(),
            [ [ 1, 1, 0, 0, ]
            , [ 1, 1, 0, 0, ]
            , [ 0, 0, 1, 1, ]
            , [ 0, 0, 1, 1, ]
            ])


if __name__ == '__main__':
    unittest.main()
