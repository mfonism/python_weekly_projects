# John Conway's Game of Life

A Python program to simulate the evolution of lives in a universe as per [John Conway's Game of Life](https://bitstorm.org/gameoflife/).

## Setup Instructions

    $ git clone https://bitbucket.org/mfonism/python_weekly_projects.git

This will download the parent repository to your computer.

cd into this project's directory.

    $ cd python_weekly_projects/game_of_life/

run the tests

    $ python -m unittest discover

Invoke the python shell

    $ python

In the shell, import the `Universe` class from the src package

    >>> from src import Universe

Instantiate a universe with an initial pattern.

    >>> pattern = { (1,1), (1,4), (2,1), (2,2), (2,3), }
    >>> uni = Universe(pattern)

Print its grid representation

    >>> print(uni.to_finite_grid())
    [
    [1, 0, 0, 1],
    [1, 1, 1, 0]
    ]

Tick the universe into evolution using its `tick` property

    >>> uni.tick()

Print the grid representation of the universe's pattern at this stage in its evolution

    >>> print(uni.to_finite_grid())
    ...
    [
    [1, 0, 1],
    [1, 1, 1],
    [0, 1, 0]
    ]

You may continue ticking the universe into successive evolutionary stages, and printing the grid representation of its pattern at each stage in the evolution.

This helper function may come in handy

    >>> def tick():
    ...     uni.tick()
    ...     print(uni.to_finite_grid())

Now you can tick away with ease

    >>> tick()
    [
    [1, 0, 1],
    [1, 0, 1],
    [1, 1, 1]
    ]
    >>> tick()
    [
    [1, 1, 0, 1, 1],
    [0, 1, 0, 1, 0],
    [0, 0, 1, 0, 0]
    ]

If you're as excited about this as I am, you'll observe that on the ninth tick, the lives form some sort of heart shape. And it gets even better on the twelfth and the thirteenth.

**Fun Fact**: Did you observe that the shape of the living world (for this initial pattern) is symmetric from the first tick?

**Extra Fun Fact**: After the 175th tick you get a [still life](https://en.wikipedia.org/wiki/Still_life_(cellular_automaton)) (an [oscillator](https://en.wikipedia.org/wiki/Oscillator_(cellular_automaton)) with a period of 1).

## Reflection

I had set out to complete this project in an hour, but completed it in three. Looking back, I realize I spent most of the time writing tests and beating the documentation into shape.

And I'm getting really good at writing documentation.

My code is properly organized, highly readable, of high quality, and the problem has been solved.
