class Bot:
    '''A bot can enter a room at a given coordinate position 
    in the room, and step in any cardinal direction in the room.'''

    def enter_room(self, room, coord):
        '''Enter room at given (x,y) coordinate.'''
        self._room = room
        self._pos  = coord
        self._cleaned_coords = set()

        self.clean_current_pos_if_dirty()

    def get_pos(self):
        return self._pos

    def step_north(self):
        (x,y) = self._pos
        self.try_stepping_and_cleaning(x, y+1)

    def step_south(self):
        (x,y) = self._pos
        self.try_stepping_and_cleaning(x, y-1)

    def step_east(self):
        (x,y) = self._pos
        self.try_stepping_and_cleaning(x+1, y)

    def step_west(self):
        (x,y) = self._pos
        self.try_stepping_and_cleaning(x-1, y)

    def try_stepping_and_cleaning(self, x, y):
        if not self._room.has_coord(x, y):
            return

        self._pos = (x, y)
        self.clean_current_pos_if_dirty()

    def clean_current_pos_if_dirty(self):
        if self.has_coord_been_cleaned(*self._pos) or not self._room.is_coord_dirty(*self._pos):
            return

        self._cleaned_coords.add(self._pos)

    def has_coord_been_cleaned(self, x, y):
        return (x, y) in self._cleaned_coords

    def get_work_done(self):
        return self._cleaned_coords

    def enter_and_clean_room_up(self, room, init_coord=(0,0), directions=""):

        direction_func_dict = { "N" : self.step_north
                              , "S" : self.step_south
                              , "E" : self.step_east
                              , "W" : self.step_west
                              , }

        self.enter_room(room, init_coord)
        for char in directions.upper().strip():
            direction_func_dict[char]()

        return (self.get_pos(), len(self.get_work_done()))
