class Room:
    '''A room is specified by a width and a height.

    The bottom right left of a room with width w and height h 
    has the coordinate (0,0)
    while its top right has the coordinate (w-1, h-1).
    '''

    def __init__(self, width, height, dirty_coords=set()):
        self._width = width
        self._height = height
        self._dirty_coords = dirty_coords

    def has_coord(self, x, y):
        return x in range(self._width) and y in range(self._height)

    def is_coord_dirty(self, x, y):
        return (x, y) in self._dirty_coords

    def __str__(self):
        return ( f'I am a {self._width} units wide and'
                 f' {self._height} units high'
                 f' 2D cartesian room with dirt at the following coordinates:'
                 f' {sorted(self._dirty_coords)}.'
                )
