# Roomba

A Python program to simulate the actions of a room cleaning bot.

## Setup Instructions

Clone the parent repository to your computer

    $ git clone https://bitbucket.org/mfonism/python_weekly_projects.git

cd into this project's directory.

    $ cd python_weekly_projects/roomba/

run the tests

    $ python -m unittest discover

Invoke the python shell

    $ python

In the shell, import the classes `Bot` and `Room` from the src package

    >>> from src import Bot, Room

Instantiate a 2D cartesian room with a width (length along the X-axis), a height (length along the Y-axis) and a set of coordinates where dirt can be found

    >>> room = Room(width=6, height=4, dirty_coords={ (0,0), (2,1), (2,3), (3,2), (4,1), (4,2), (5,0), (5,2), })

Feel free to print it to screen :)

    >>> print(room)
    I am a 6 units wide and 4 units high 2D cartesian room with dirt at the following coordinates: [(0, 0), (2, 1), (2, 3), (3, 2), (4, 1), (4, 2), (5, 0), (5, 2)].

Instantiate a bot

    >>> roomba = Bot()

Send the bot into the room with a list of directions, to start cleaning the room up from an initial coordinate

    >>> bot.enter_and_clean_room_up(room, init_coord=(3,2), directions="NWSSEENNE")

That should respond with the tuple `((5,3), 5)`, indicating that the bot followed your directions from the initial coordinate you had supplied, and ended up at the coordinate (5,3), cleaning up 5 spots in the process.

## Reflection

While carrying out this project, I observed a pattern which I had observed in my previous project (John Conway's Game of Life): the creation of a two dimensional grid of cells where the state of the cells is binary in nature.

In the previous project, it was the LIVING | NON_LIVING binary, while in this project it was the DIRTY | NOT_DIRTY binary.

And just like I did in the previous project, I implemented this 2D grid efficiently by 'not caring about' the locations existing in one of the states (the NOT_DIRTY state, in this case).

This project has reinforced this problem solving approach in me. I am confident I will always recognize cases when this approach is the most efficient, and make use of it.

## Parting Words

This is definitely not the end!
