import unittest

from src.bot import Bot
from src.room import Room


class BotTest(unittest.TestCase):

    def setUp(self):
        self.roomba = Bot()
        self.roomba.enter_room(Room(width=10, height=5), (8,3))

    def test_step_north(self):
        self.assertEqual(self.roomba.get_pos(), (8,3))

        self.roomba.step_north()
        self.assertEqual(self.roomba.get_pos(), (8,4))

    def test_step_south(self):
        self.assertEqual(self.roomba.get_pos(), (8,3))

        self.roomba.step_south()
        self.assertEqual(self.roomba.get_pos(), (8,2))

    def test_step_east(self):
        self.assertEqual(self.roomba.get_pos(), (8,3))

        self.roomba.step_east()
        self.assertEqual(self.roomba.get_pos(), (9,3))

    def test_step_west(self):
        self.assertEqual(self.roomba.get_pos(), (8,3))

        self.roomba.step_west()
        self.assertEqual(self.roomba.get_pos(), (7,3))


class BotWallRecognitionTest(unittest.TestCase):

    def test_cant_step_over_northern_wall(self):
        roomba = Bot()
        roomba.enter_room(Room(width=3, height=5), (1,4))
        roomba.step_north()

        self.assertEqual(roomba.get_pos(), (1,4))

    def test_cant_step_under_southern_wall(self):
        roomba = Bot()
        roomba.enter_room(Room(width=3, height=5), (1,0))
        roomba.step_south()

        self.assertEqual(roomba.get_pos(), (1,0))

    def test_cant_step_beyond_eastern_wall(self):
        roomba = Bot()
        roomba.enter_room(Room(width=3, height=5), (2,1))
        roomba.step_east()

        self.assertEqual(roomba.get_pos(), (2,1)) 

    def test_cant_step_beyond_western_wall(self):
        roomba = Bot()
        roomba.enter_room(Room(width=3, height=5), (0,2))
        roomba.step_west()

        self.assertEqual(roomba.get_pos(), (0,2))


class BotAsAJanitorTest(unittest.TestCase):

    def setUp(self):
        dirty_coords = {
            (0, 3), (1, 3),
                    (1, 2), (2, 2),
                                    (3, 1),
                    (1, 0),
        }
        self.room = Room(width=4, height=4, dirty_coords=dirty_coords)
        self.roomba = Bot()

    def test_bot_cleans_up_first_entry_cell_if_dirty(self):
        self.roomba.enter_room(self.room, (2,2))
        self.assertEqual(self.roomba.get_work_done(), {(2,2)})

    def test_bot_doesnt_clean_up_first_entry_cell_if_clean(self):
        self.roomba.enter_room(self.room, (0,0))
        self.assertEqual(self.roomba.get_work_done(), set())

    def test_bot_cleans_up_new_position_on_stepping_if_dirty(self):
        self.roomba.enter_room(self.room, (3,3))
        self.assertEqual(self.roomba.get_work_done(), set())

        self.roomba.step_west()
        self.assertEqual(self.roomba.get_work_done(), set())

        self.roomba.step_west()
        self.assertEqual(self.roomba.get_work_done(), {(1,3)})

        self.roomba.step_west()
        self.assertEqual(self.roomba.get_work_done(), {(1,3), (0,3)})

        self.roomba.step_south()
        self.assertEqual(self.roomba.get_work_done(), {(1,3), (0,3)})


class CleanRoomWithDirectionsTest(unittest.TestCase):

    def setUp(self):
        self.room = Room(width=6,
                         height=4,
                         dirty_coords={ (0,0)
                                      , (2,1)
                                      , (2,3)
                                      , (3,2)
                                      , (4,1)
                                      , (4,2)
                                      , (5,0)
                                      , (5,2)
                                      , }
                        )
        self.roomba = Bot()

    def test_enter_and_clean_room_up(self):
        final_coord, num_work_done = self.roomba.enter_and_clean_room_up(self.room, init_coord=(3,2), directions="NWSSEENNE")

        self.assertEqual(final_coord, (5,3))
        self.assertEqual(num_work_done, 5)


if __name__ == "__main__":
    unittest.main()
