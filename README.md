# Python Weekly Projects

## Preamble ##
April 2019.

I just got a smartphone and a bearable laptop. And I'm so passionately *over the moon* about it.

In the dark days past, when my phone was not smart and my laptop was super fragile, I read a lot. I read a lot of Python, Haskell and Smalltalk.

And because I survived (and, in deed, thrived in) an educational system where students almost never got to have a practice experience of the theory they were learning, it was not too hard to convince myself on the workings of my own solutions to exercises in the books I read.

Especially for Haskell, because that one really comes off like mathematics. And I love mathematics.

From time to time, I would have a roommate whose laptop was less frustrating to use than mine, and I would install a few software on it so I could try out the beautiful pieces of codes in my mind.

But that only happened from time to time.

Besides, there's only so much software you can install on someone else's device before the owner starts making sure every day is World Password Day.

In those dark days, I always told myself, *as soon as I get to the end of this tunnel...!*

Now I'm in the light. And I'm firing on all cylinders. I am confident I will catch up with all I missed.

I'm focusing on Python because besides being an awesome language to solve problems in, it allows me to think like a Smalltalker, as well as a Haskeller (and I have nothing but praise for those two languages).

In order to level-up in Python I will be working on one simple project a week. I will be expanding my problem solving abilities, honing my TDD skills and learning how to git.

I am super positive, highly motivated, and overflowing with energy for this adventure.

Yes, I can!

---

## The Projects

1. **poker_hand_comparison**
2. **twenty_forty_eight**
3. **game_of_life**
4. **roomba**
5. **connect_n_checker**
